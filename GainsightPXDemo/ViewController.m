//
//  ViewController.m
//  GainsightPXDemo
//
//  Created by Adam Hass on 3/26/19.
//  Copyright © 2019 AppLink.io. All rights reserved.
//

#import "ViewController.h"
#import "FeatureViewController.h"
#import "AppLinkIO/AppLinkIO.h"

@interface ViewController ()

@end

@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]) {
        // Do Not Show On Subsequent Launches
    } else { 
        // Show Welcome Message If App Launched First Time
        [AppLinkIO triggerInAppMessage:@"6084229B-77B4-DD52-6432-1E51BFF8CAF4"];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
}


-(void)dismissKeyboard {
    [self.view endEditing:YES];
    
}

- (IBAction)loginAction:(id)sender {
    
    BOOL errorFree = NO;
    
    if (self.firstNameTextField.text && self.firstNameTextField.text.length > 0) {
        errorFree = YES;
        self.firstNameTextField.layer.borderWidth=0.0;
    } else {
        errorFree = NO;
        self.firstNameTextField.layer.borderColor=[[UIColor redColor]CGColor];
        self.firstNameTextField.layer.borderWidth=1.0;
    }
    
    if (self.lastNameTextField.text && self.lastNameTextField.text.length > 0) {
        errorFree = YES;
        self.lastNameTextField.layer.borderWidth=0.0;
    } else {
        errorFree = NO;
        self.lastNameTextField.layer.borderColor=[[UIColor redColor]CGColor];
        self.lastNameTextField.layer.borderWidth=1.0;
    }
    
    if (self.emailTextField.text && self.emailTextField.text.length > 0) {
        errorFree = YES;
        
        self.emailTextField.layer.borderWidth=0.0;
        BOOL validEmailAddress;
        NSString *Regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,16}";
        NSPredicate *TestResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
        validEmailAddress = [TestResult evaluateWithObject:self.emailTextField.text];
        
        if(!validEmailAddress) {
            errorFree = NO;
            self.emailTextField.layer.borderColor=[[UIColor redColor]CGColor];
            self.emailTextField.layer.borderWidth=1.0;
        }
        
    } else {
        errorFree = NO;
        self.emailTextField.layer.borderColor=[[UIColor redColor]CGColor];
        self.emailTextField.layer.borderWidth=1.0;
    }
    
    // Start New Analytics Session
    [AppLinkIO startSession]; 
    
    // Associating Session with Provided Email Address
    [AppLinkIO linkUser:self.emailTextField.text byType:@"email"];
    
    // Adding First and Last Name to User Record
    [AppLinkIO setUserAttribute:@"FirstName" withValue:self.firstNameTextField.text];
    [AppLinkIO setUserAttribute:@"LastName" withValue:self.lastNameTextField.text];
    
    if(errorFree) {
        
        NSDictionary *eventAttributes = @{@"Category" : @"Authentication" };
        [AppLinkIO trackEvent:@"User Logged In" withAttributes:eventAttributes];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FeatureViewController *featureViewController = [storyboard instantiateViewControllerWithIdentifier:@"FeatureViewController"];
        [self presentViewController:featureViewController animated:NO completion:nil];
        
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsLoggedIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end
