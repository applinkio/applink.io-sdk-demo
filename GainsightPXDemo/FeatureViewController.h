//
//  FeatureViewController.h
//  GainsightPXDemo
//
//  Created by Adam Hass on 3/26/19.
//  Copyright © 2019 AppLink.io. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeatureViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tableView;
}
 

@end

NS_ASSUME_NONNULL_END
