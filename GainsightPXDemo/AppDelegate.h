//
//  AppDelegate.h
//  GainsightPXDemo
//
//  Created by Adam Hass on 3/26/19.
//  Copyright © 2019 AppLink.io. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

