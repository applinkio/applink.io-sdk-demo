//
//  FeatureViewController.m
//  GainsightPXDemo
//
//  Created by Adam Hass on 3/26/19.
//  Copyright © 2019 AppLink.io. All rights reserved.
//

#import "FeatureViewController.h"
#import "ViewController.h"
#import "AppLinkIO/AppLinkIO.h"

@interface FeatureViewController ()

@end

@implementation FeatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    tableView=[[UITableView alloc]init];
    
    tableView.frame = self.view.bounds;
    tableView.dataSource=self;
    tableView.delegate=self;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [tableView reloadData];
    [self.view addSubview:tableView];
    
    
}


-(void)onTapDone:(UIBarButtonItem*)item{
    
}

-(void)onTapCancel:(UIBarButtonItem*)item{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier   forIndexPath:indexPath] ;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.row == 0) {
        cell.textLabel.text = @"Trigger In App Message";
    } else if(indexPath.row == 1) {
        cell.textLabel.text = @"Track Screen View";
    } else if(indexPath.row == 2) {
        cell.textLabel.text = @"Track Screen View With Attributes";
    } else if(indexPath.row == 3) {
        cell.textLabel.text = @"Track Event";
    } else if(indexPath.row == 4) {
        cell.textLabel.text = @"Track Event View With Attributes";
    } else if(indexPath.row == 5) {
        cell.textLabel.text = @"Set User Reserved Attribute";
    } else if(indexPath.row == 6) {
        cell.textLabel.text = @"Set User Custom Attribute";
    } else if(indexPath.row == 7) {
        cell.textLabel.text = @"Increment User Attribute";
    } else if(indexPath.row == 8) {
        cell.textLabel.text = @"Decrement User Attribute";
    } else if(indexPath.row == 9) {
        cell.textLabel.text = @"Log Out";
        cell.textLabel.textColor = [UIColor redColor];
    } else {
        cell.textLabel.text = @"Extra";
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == 0) {
        // Trigger In App Message
        [AppLinkIO triggerInAppMessage:@"EBBA60FC-1FFE-91E9-6B69-ACEAAC977720"];
        
    } else if(indexPath.row == 1) {
        
        // Track Screen View
        [AppLinkIO trackScreenView:@"Feature View Example"];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Screen View Tracked" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 2) {
        
        // Track Screen View with Attributes
        NSDictionary *screenAttributes = @{@"Category" : @"Feature Usage"};
        [AppLinkIO trackScreenView:@"Feature View Example 2" withAttributes:screenAttributes];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Screen View with Attributes Tracked"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 3) {
        // Track Event
        [AppLinkIO trackEvent:@"Event Track Example"];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Event Tracked" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 4) {
        // Track Event with Attributes
        NSDictionary *evemtAttributes = @{@"Category" : @"Feature Usage"};
        [AppLinkIO trackEvent:@"Event Track Example 2" withAttributes:evemtAttributes];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Screen View with Attributes Tracked"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 5) {

        // Set User Reserved Attribute
        [AppLinkIO setUserAttribute:@"Phone" withValue:@"+18886238562"];
       
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Phone Number Attribute Set"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
    } else if(indexPath.row == 6) {
        
        // Set User Custom Attribute
        [AppLinkIO setUserAttribute:@"Account_Type" withValue:@"SDK Test"];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Account Type User Attribute Set"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 7) {
        
        // Increment User Attribute
        [AppLinkIO incrementUserAttribute:@"Account_Number" withValue:@1];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Account Number Attribute Incremented"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 8) {
        
        // Decrement User Attribute
        [AppLinkIO decrementUserAttribute:@"Account_Number" withValue:@1];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AppLink.io SDK Notice"
                                                                                 message:@"Account Number Attribute Decremented"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else if(indexPath.row == 9) {
        
        // Track Log Out Event
        NSDictionary *eventAttributes = @{@"Category" : @"Authentication" };
        [AppLinkIO trackEvent:@"User Logged Out" withAttributes:eventAttributes];
         
        // Log Out User
        [AppLinkIO unlinkUser];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IsLoggedIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
}


@end
